class User < ActiveRecord::Base
		has_many :photos, :dependent => :destroy

	validates :name, presence: true
	#validates :photos, presence: true
	validates_associated :photos
	accepts_nested_attributes_for :photos, allow_destroy: true
end
