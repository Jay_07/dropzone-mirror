class Photo < ActiveRecord::Base
	belongs_to :user
	
	mount_uploader :image, ImageUploader

	validates :user, presence: true
	validate :image_size_validation
  	validates :image ,
				:presence => true

  private
  
  def image_size_validation
    errors[:image] << "should be less than 4MB" if image.size > 4.megabytes
  end
	
end
