class PhotosController < ApplicationController
 attr_accessible :remove_image,:image
  def new
  	@photo = Photo.new
  end

  def destroy
    @photo.destroy
  end

  def create
  	@photo = Photo.new(photo_params)
  	@photo.save
  end

  private 
  def photo_params
  	      params.require(:photo).permit(:image)
  end
end
